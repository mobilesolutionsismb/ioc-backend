# I-REACT Open Core Backend

This is the repository of the main releases of the [I-REACT Open Core](https://mobilesolutionsismb.bitbucket.io/i-react-open-core/) Backend

## Acknowledgment

This work was partially funded by the European Commission through the [I-REACT project](http://www.i-react.eu/) (H2020-DRS-1-2015), grant agreement n.700256.

## Freeware Software

Because of the license of the framework with which it was implemented, it is not possible to release its source code.

It was possible, however, to release it as freeware, as a binary release you are free to deploy where you need, especially if you want to use one of the other [I-REACT Open Core projects](https://bitbucket.org/account/user/mobilesolutionsismb/projects/IROC) which require it as a dependency.

The binary release [can be downloaded here](https://bitbucket.org/mobilesolutionsismb/ioc-backend/raw/997c9d7bf9b5119e113dbd5df132364ef2d71d7e/release/ioc-backend.zip). It contains all instructions and licenses.
